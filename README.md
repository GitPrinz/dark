# Dark Series

Just some of my notes.

Major Spoilers ahead.

## Overview

Watch and Edit at https://mermaid.live/

Status
- incomplete
- mostly first world
- unclude some errors for sure

Opportunities to Improve
- [ ] More Colors
- [ ] Complete Second World Graph

```mermaid
graph LR

classDef subsub fill:#aef
classDef medal fill:#37f
classDef akw fill:#990
classDef book fill:#950

subgraph Legend
    Mother{{Mother}} --> x_birth[/Birth\]
    Father{{Father}} --> x_birth[/Birth\]
    x_birth ==> x_young[/young Person/]
    x_young ==> x_middle{{Person}}
    x_middle ==> x_old[\old Person\]
    x_old ==> x_death[\Death by ../]
end
class Legend subsub

%% People %%
%%%%%%%%%%%%

%% Noah & Agnes
noah_birth[/Noah, Agnes\] ==> noah_young[\Noah\]
noah_birth ==> agnes_middle{{Agnes}}
noah_young ==> noah_middle{{Noah}}
noah_middle ==> noah_death[\shot/]
agnes_middle --> noah_death

%% Helge Doppler
psycho_middle --> helge_birth
akw_middle --> helge_birth[/Helge Doppler\] ==> helge_young[/young Helge/]
helge_young ==> helge_middle{{Helge}}
helge_middle ==> helge_old[\Helge Old\]
helge_old ==> helge_death[\?/]

%% akw
akw_middle{{AKW Chef}} ==> akw_old[\old AKW Chef\]

%% Peter Doppler
unknown_wife{{Helges Wife}} --> peter_birth
helge_middle --> peter_birth[/Peter\]
peter_birth ==> peter_middle{{Peter}}
peter_middle ==> peter_middle_apo{{Peter}}
peter_middle_apo ==> peter_death[\fight with stranger/]

%% Charlotte
elisabeth_middle --> charlotte_birth
noah_middle --> charlotte_birth[/Charlotte\]
charlotte_birth ==> charlotte_young[/Young Charlotte/]
charlotte_young ==> charlotte_middle{{Polizistin Charlotte}}

%% Polizistin Familie
charlotte_middle --> doppler_birth
peter_middle --> doppler_birth[/Franziska, Elisabeth\]
doppler_birth ==> franziska_young[/Franziska/]
franziska_young ==> franziska_middle{{Franzsika}}
charlotte_middle --> doppler_birth
peter_middle --> doppler_birth
doppler_birth ==> elisabeth_young[/Fuchs Elisabeth/]
elisabeth_young ==> elisabeth_young_apo[/Elisabeth/]
elisabeth_young_apo ==> elisabeth_middle{{Leader Elisabeth}}

%% Helene Albers
helene_young[/young Helene/] ==> helene_middle{{Krankenhaus Reception Helene}}

%% Katharina Albers / Niehlsen
helene_middle --> katharina_birth
katharina_birth[/Katharina Albers\] ==> katharina_young[/Bully Katharina/]
katharina_young ==> katharina_middle{{Schulleiterin Katharina}}
katharina_middle --> nielsen_birth
katharina_middle ==> katharina_middle_time{{Angry Katharina}}
katharina_middle_time ==> katharina_death[\stone/]

%% Tronte Nielsen
agnes_middle --> tronte_birth
dreier_middle -->|?| tronte_birth[/Tronte Nielsen\]
tronte_birth ==> tronte_young[/Young Tronte/]
tronte_young ==> tronte_middle{{Tronte}}

%% Jana
jana_young[/young Jana Grausträn/] ==> jana_middle{{Jana Grausträn}}

%% Ulrich Nielsen
jana_middle --> ulrich_birth
tronte_middle --> ulrich_birth[/Ulrich, Mads\]
ulrich_birth ==> ulrich_young[/young Ulrich/]
ulrich_young ==> ulrich_middle{{Police Ulrich}}
ulrich_middle ==> ulrich_middle_time{{Crazy Ulrich}}
ulrich_middle_time ==> ulrich_old[\Ulrich the grey\]
ulrich_middle --> nielsen_birth[/Mikkel, Martha, Magnus\]

%% Mads Niehlsen
ulrich_birth ==> mads_young[/Mads/]
mads_young ==> mads_death[\time machine/]

%% Mikkel Niehlsen
nielsen_birth ==> mikkel_young[/Mikkel/]
mikkel_young ==> mikkel_young_time[/young Michael alias Mikkel/]
mikkel_young_time ==> mikkel_middle{{Michael Kahnwald alias Mikkel}}
mikkel_middle ==> mikkel_death[\suicide/]

%% Magnus Nielsen
nielsen_birth ==> magnus_young[/Magnus/]
magnus_young ==> magnus_middle{{Magnus}}

%% Ines Kahnwald
ines_middle{{Ines Nurse}} .-> mikkel_young_time

%% Jonas Kahnwald
hanna_middle --> jonas_birth
mikkel_middle --> jonas_birth[/Jonas\]
jonas_birth ==> jonas_young[/Gelbjäckchen Jonas/]
jonas_young ==> jonas_middle{{Streicher Jonas}}
jonas_middle ==> jonas_middle_time{{Handsom Jonas}}
jonas_middle_time ==> jonas_old[\Adam\]

%% Egon Tiedemann
egon_middle{{Polizist Egon}} ==> egon_old[/Renter Egon/]
egon_old ==> egon_death[\streit/]
claudia_middle --> egon_death

%% Claudia Tiedemann
doris_middle{{Shy Doris}} --> claudia_birth
egon_middle --> claudia_birth[/Claudia\]
claudia_birth ==> claudia_young[/young Claudia/]
claudia_young ==> claudia_middle{{AKW Claudia}}
claudia_middle ==> claudia_middle_apo{{Apokalypse Claudia}}
claudia_middle_apo ==> claudia_old[\Weißer Teufel Claudia\]
claudia_middle_apo --> claudia_middle_apo_b_death[\gun/]

%% Regina Tiedemann
tronte_middle -->|?| regina_birth
claudia_middle --> regina_birth[/Regina\]
regina_birth ==> regina_young[/Brillen? Regina/]
regina_young ==> regina_middle{{Hotel Regina}}
regina_middle ==> regina_death[\cancer and pillow/]

%% Hanna
hanna_young[/Hanna/] ==> hanna_middle{{Hanna}}
hanna_middle ==>|Zeitreise| hanna_middle_time{{Hanna}}
hanna_middle_time ==> hanna_death[\Pillow/]
jonas_old --> hanna_death

%% Sila & Bartosch
hanna_middle_time --> sila_birth
egon_middle --> sila_birth
sila_birth ==> sila_middle
sila_middle --> noah_birth
bartosch_middle_time --> noah_birth

%% Bartosch
regina_middle --> bartosch_birth
alexander_middle --> bartosch_birth[/Bartosch\]
bartosch_birth ==> bartosch_young[/Stress Bartosch/]
bartosch_young ==> bartosch_middle{{Bartosch}}
bartosch_middle ==> bartosch_middle_time{{Bartosch}}

%% Dreier
martha_young_b[Martha B] --> dreier_birth
jonas_young --> dreier_birth[/Dreifaltigkeit\]
dreier_birth ==> dreier_young[/Dreifaltigkeit/]
dreier_young ==> dreier_middle{{Dreifaltigkeit}}
dreier_middle ==> dreier_old[\Dreifaltigkeit\]

%% Uhrmacher
uhr_young[/Young Uhrmacher/] ==> uhr_middle{{Uhrmacher}}
uhr_middle ==> uhr_old[\old Uhrmacher?\]
uhr_middle --> uhr_daughter
uhr_daughter ==> uhr_daugther_death[\car crash/]
uhr_daughter --> uhr_child

%% Alexander
alexander_young[/Alexander Gun/] ==> alexander_middle{{Alexander AKW}}



%% Years %%
%%%%%%%%%%%

subgraph 1887
    franziska_middle
    magnus_middle
    jonas_middle_time
    bartosch_middle_time
    hanna_death
end

subgraph 1920 Secret Lair
    noah_birth
    noah_young
    noah_death
    jonas_old
end

subgraph 1953
    noah_middle
    psycho_middle
    akw_middle
    uhr_young
    helge_birth
    ulrich_middle_time .->|hitting| helge_young
    tronte_birth
    tronte_young
    claudia_birth
    claudia_young .->|Nachhilfe| helge_young
    egon_middle .-> amulett_x1
    amulett_x1[(Christopherus)]:::medal .-> hanna_middle_time
    hanna_middle_time .-> amulett_x2[(Christopherus)]:::medal
    amulett_x2 .-> helene_young
    agnes_middle -.-|love| doris_middle
end

subgraph 1986
    jana_middle
    tronte_middle
    ulrich_birth
    ulrich_old
    mikkel_young_time
    ines_middle
    unknown_wife
    helge_middle
    peter_birth
    %% peter_young
    katharina_birth
    helene_middle .->|lost in fight at See|amulett_86[(Christopherus)]:::medal
    katharina_middle_time
    helene_middle -->|fight| katharina_death
    helene_middle .->|beat| katharina_young
    charlotte_birth
    uhr_middle .->|cares for| charlotte_young
    akw_old .-> akw_lead[(AKW)]:::akw
    akw_lead .-> claudia_middle
    regina_birth
    alexander_young .-|friends| regina_young
    subgraph 86kids
        regina_young
        hanna_young
        ulrich_young
        katharina_young
    end
    class 86kids subsub
    mads_young
    helene_middle
    mads_death
    egon_old
    egon_death
end
amulett_86 .-> amulett_19
subgraph 2019
    jonas_birth
    nielsen_birth
    mikkel_middle
    mikkel_death
    franziska_birth
    elisabeth_birth
    bartosch_birth
    subgraph 19kids
        mikkel_young
        franziska_young
        elisabeth_young
        magnus_young
        jonas_young
        martha_young
        bartosch_young
    end
    class 19kids subsub
    regina_middle
    katharina_middle
    hanna_middle
    ulrich_middle
    charlotte_middle
    peter_middle .-> peter_middle_note([A Therapeut])
    peter_middle .-> peter_middle_note_b([B Priester])
    helge_old
    amulett_19[(Christopherus)]:::medal .->|gefunden| jonas_young
    mads_death_note[[Mads found in forrest]]
    claudia_middle
end

mads_death .-> mads_death_note

subgraph After Apokalyps
    regina_death
    jonas_middle
    claudia_middle_b .-> book_a[(Notizbuch)]:::book
    book_a .-> claudia_middle_apo
    martha_young_b .->|weggenommen| amulett_z[(Christopherus)]:::medal
    amulett_z .-> jonas_old
    dreier_birth
    elisabeth_middle
    elisabeth_young_apo -.-|living togehter| peter_middle_apo
    peter_death
end
```

## That is all

Do what you want with it ;)
